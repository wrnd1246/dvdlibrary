/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvd.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ObjectNotFoundException;

/**
 *
 * @author user
 */
public class DVDLibraryInterface implements Serializable{
    private String dataDir; // data directory
    private static final List<DVDItem> DVD_CACHES = new LinkedList<>();
    
    public DVDLibraryInterface(String dataDir){
        this.dataDir = dataDir;
        
        synchronized (DVD_CACHES){
            if (DVD_CACHES.isEmpty()) {
                cacheDVD();
            }
        }
        
    }
    private  void cacheDVD(){
        String fullFilename = dataDir + "library.txt";
        BufferedReader br = null;
        try{
            br = new BufferedReader(new FileReader(fullFilename));
            String line;
            while((line = br.readLine()) != null){
                String [] fields = line.split(",\\s+");
                int objectID = Integer.parseInt(fields[0]);
                String title = fields[1];
                int year = Integer.parseInt(fields[2]);
                String genre = fields[3];
                String type = fields[4];
                
                DVDItem d = new DVDItem(objectID, title, year, genre, type);
                
                DVD_CACHES.add(d);
                
            }
        } catch (Exception e) {
            System.err.println(e);
        }finally{
            if(br != null){
                try{
                    br.close();
                }catch(IOException e){
                    System.err.println(e);
                }
            }
        } 
    }

    
    
    public DVDItem addDVD(String title, int year, String genre, String type){
        int nextID = DVD_CACHES.size() + 1;
        
        DVDItem d = new DVDItem(nextID, title, year, genre, type);
        
        storeDVD(d);
        
        DVD_CACHES.add(d);
        return d;
    }
    
    private void storeDVD(DVDItem d){
        String fullFilename = dataDir+"library.txt";
        PrintWriter pw = null;
        try{
            pw = new PrintWriter(new FileWriter(fullFilename, true));
            
            pw.print(d.objectID);
            pw.print(", ");
            pw.print(d.title);
            pw.print(", ");
            pw.print(d.year);
            pw.print(", ");
            pw.print(d.genre);
            pw.print(", ");
            pw.print(d.type);
            pw.println();
        }catch (Exception e) {
            System.err.println(e);
        }finally{
            if(pw != null){
                try{
                    pw.close();
                }catch(Exception e){
                    System.err.println(e);
                }
            }
        } 
    }
    
    public List<DVDItem> getDVDColletion(){
        return Collections.unmodifiableList(DVD_CACHES);
    }
    
    public DVDItem getGenre(String genre) throws ObjectNotFoundException{
        for (DVDItem d : DVD_CACHES) {
            if (genre.equals(d.getGenre())) {
                return d;
            }
        }
        throw new ObjectNotFoundException();
    }
    /*
    public void addGenre(String genre){
        boolean b_genre = false;
        for (DVDItem d : DVD_CACHES) {
            if (genre.equals(d.getGenre())) {
                b_genre = true;
            }
        }
        if (b_genre == false) {
            
        } 
    }*/
    
}
