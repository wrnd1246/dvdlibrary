/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvd.model;

/**
 *
 * @author user
 */
public class DVDItem implements java.io.Serializable{
    String title;
    int year;
    String genre;
    int objectID;
    String type;

    public DVDItem(String title, int year, String genre) {
        this.title = title;
        this.year = year;
        this.genre = genre;
    }
    public DVDItem(int objectID, String title, int year, String genre, String type) {
        this.title = title;
        this.year = year;
        this.genre = genre;
        this.objectID = objectID;
        this.type = type;
    }
//    public DVDItem(String type) {
//        
//    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    
    
    public int getObjectID() {
        return objectID;
    }
   

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getGenre() {
        return genre;
    }
    
    
}
