/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvd.view;

import com.dvd.model.DVDItem;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ListLibraryServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private ArrayList<DVDItem> dvds;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List dvds = (List) getServletContext().getAttribute("dvdList");
        HttpSession session = request.getSession();
        ArrayList list_I = new ArrayList();
        ArrayList list_T = new ArrayList();
        ArrayList list_Y = new ArrayList();
        ArrayList list_G = new ArrayList();
        ArrayList list_img = new ArrayList();

        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        session.setAttribute("dvds_size", dvds.size());

        Iterator it = dvds.iterator();
        while (it.hasNext()) {
            DVDItem Item = (DVDItem) it.next();
            list_I.add(Item.getObjectID());

            try {
                if (session.getAttribute("showTitle") == "true") {
                    list_T.add(Item.getTitle());
                    list_img.add(Item.getTitle() + Item.getType());
                }
            } catch (Exception e) {
            }

            try {
                if (session.getAttribute("showYear") == "true") {
                    list_Y.add(Item.getYear());
                }
            } catch (Exception e) {
            }

            try {
                if (session.getAttribute("showGenre") == "true") {
                    list_G.add(Item.getGenre());
                }
            } catch (Exception e) {
            }

        }
        session.setAttribute("list_I", list_I);
        session.setAttribute("list_T", list_T);
        session.setAttribute("list_Y", list_Y);
        session.setAttribute("list_G", list_G);
        session.setAttribute("list_img", list_img);
        request.getRequestDispatcher("ListDVDs.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
