/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvd.view;

import com.dvd.model.DVDItem;
import com.img.controller.CreateRandImg;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
public class AddDVDFormServlet extends HttpServlet {

    private String[] genres;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        List errMsg = (List) request.getAttribute("errMsg");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Add DVD</title>");
            out.println("</head>");
            out.println("<body>");
            if (errMsg.size() > 0) {
                out.println("<font color = red>Please correct the following errors:<br/><br/>");
                Iterator item = errMsg.iterator();
                while (item.hasNext()) {
                    String message = (String) item.next();
                    out.println("<li>" + message + "</li>");
                }
                out.println("</font>");
            }

            out.println("<form action=\"add_dvd.do\" method=\"POST\">");
            String title = request.getParameter("title");
            if (title == null) {
                title = "";
            }
            out.println("<p>Title: <input type=\"text\" name=\"title\" value ='"
                    + title + "' /></p>");

            String year = request.getParameter("year");
            if (year == null) {
                year = "";
            }
            out.println("<p>Year: <input type=\"text\" name=\"year\" value= '"
                    + year + "'/></p>");

            String o_genre = request.getParameter("o_genre");
            out.println("Genre: <select name='o_genre'>");
            for (int i = 0; i < genres.length; i++) {
                out.print("<option");
                if (genres[i].equals(o_genre)) {
                    out.print(" selected");
                }
                out.println(">" + genres[i] + "</option>");
            }
            out.println("</select>");

            String n_genre = request.getParameter("n_genre");
            if (n_genre == null) {
                n_genre = "";
            }
            out.println("or new genre: <input type=\"text\" name=\"n_genre\" value= '" + n_genre + "'/></p>");

            out.println("checkCode:<img src='CreateRandImg.view'><br/>");
            out.println("please input the checkCode:<input type='text' name='code'><br/>");

            out.println("<input type=\"submit\" value=\"Add DVD\" />");
            out.println("</form>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        String genreStr = getInitParameter("genres");
        if (genreStr == null) {
            genreStr = "Sci-Fi,Drama,Comedy";
        }
        genres = genreStr.split(",\\s*");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
