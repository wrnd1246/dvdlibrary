/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvd.controller;

import com.dvd.model.DVDItem;
import com.dvd.model.DVDLibraryInterface;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class AddDVDAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        List errMsg = new ArrayList();
        request.setAttribute("errMsg", errMsg);
        HttpSession session = request.getSession();
        try {
            //1

            String title = request.getParameter("title").trim();
            String yearStr = request.getParameter("year").trim();
            String genre = request.getParameter("genre").trim();
            String inputCode = request.getParameter("code").trim();

            //2            
            if (title.length() == 0) {
                errMsg.add("Please enter the title of the DVD.");
            }
            
            int year = -1;
            try {
                year = Integer.parseInt(yearStr);
            } catch (NumberFormatException e) {
                errMsg.add("The 'year' field must be a positive integer.");
            }
            //3

            if ("UNKNOW".equals(genre)) {
                errMsg.add("Please select the genre of the DVD.");
            }

            if ((year != -1) && ((year < 2000)) || (year > 2010)) {
                errMsg.add("The 'year' field must within 2000 to 2010.");
            }

            
            //驗證碼
            String code = (String) request.getSession().getAttribute("rand");
            if (!inputCode.equals(code)) {
                errMsg.add("Authcode error! please check your code again.");
            }

            if (errMsg.size() > 0) {
                return mapping.findForward("error");
            }
            //4

            DVDItem DVDItem = new DVDItem(title, year, genre);
            session.setAttribute("DVDItem", DVDItem);
            session.setAttribute("dvd_name_only", DVDItem.getTitle());
            //5
            return mapping.findForward("success");

        } catch (RuntimeException e) {
            return mapping.findForward("error");
        }
    }
}
