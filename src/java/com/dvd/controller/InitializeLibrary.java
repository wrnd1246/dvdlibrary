/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dvd.controller;

import com.dvd.model.DVDItem;
import com.dvd.model.DVDLibraryInterface;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author user
 */
public class InitializeLibrary implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        ServletContext context = sce.getServletContext();
        String libraryFile = context.getRealPath("/")+ context.getInitParameter("libraryFile");
        DVDLibraryInterface dli = new DVDLibraryInterface(libraryFile);
        List<DVDItem> dvdList = dli.getDVDColletion();
        
        
        context.setAttribute("DVDLibraryInterface", dli);
        context.setAttribute("dvdList", dvdList);
        
       /* 
        InputStream s = null;
        //BufferedReader br = null;//緩衝區讀取器
        List<DVDItem> dvds = new ArrayList<>();
        
        s = sc.getResourceAsStream(libraryFile);
        
        try(BufferedReader br = new BufferedReader(new InputStreamReader(s))){//io
            String line = null;
            while((line = br.readLine()) != null){
                String [] fields = line.split(",\\s*");
                String title = fields[0];
                int year = Integer.parseInt(fields[1]);
                String genre = fields[2];
                DVDItem dvd = new DVDItem(objectID, title, year, genre);
                dvds.add(dvd);
            }
            sc.setAttribute("dvds",dvds);
            //log
            sc.log("Read data from file completely.");
        }catch(IOException e){
            sc.log("Read data failed.");
            sc.log(e.getMessage());
        }
        */
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        sce.getServletContext().log("Appliction has Destroied");
    }
    
}
