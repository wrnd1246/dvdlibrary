package com.dvd.upload;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.dvd.model.DVDItem;
import com.dvd.model.DVDLibraryInterface;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author user
 */
public class FileUploadServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    private ServletContext sc;
    private String savePath;

    public void init(ServletConfig config) { 
        savePath = "image/";
        sc = config.getServletContext();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        
        try {
            List items = upload.parseRequest(request);
            Iterator itr = items.iterator();
            while (itr.hasNext()) {
                FileItem item = (FileItem) itr.next();
                if (item.getName() != null && !item.getName().equals("")) {
                    
                    // 上傳檔案的儲存路徑 
                    String dvd_name_only = (String) session.getAttribute("dvd_name_only");
                    String formatName = item.getName().toString().substring(item.getName().toString().length() - 4, item.getName().toString().length());
                    File file = new File(sc.getRealPath("/") + savePath, dvd_name_only + formatName);
                    DVDItem DVDItem = (DVDItem) session.getAttribute("DVDItem");
                    item.write(file);
                    
                    session.setAttribute("dvd_name", dvd_name_only + formatName);

                    
                    DVDLibraryInterface dli = (DVDLibraryInterface) request.getServletContext().getAttribute("DVDLibraryInterface");
                    dli.addDVD(DVDItem.getTitle(), DVDItem.getYear() , DVDItem.getGenre(), formatName);
                    
                    
                    //session.setAttribute("upload.name", item.getName());
                    request.setAttribute("upload_message", "上傳圖片成功！");
                } else {
                    request.setAttribute("upload_message", "沒有選擇圖片！");
                }
            }
        } catch (FileUploadException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("upload_message", "上傳失敗！");
        }
        request.getRequestDispatcher("uploadResult.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
