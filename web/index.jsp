<%-- 
    Document   : index
    Created on : 2016/1/4, 下午 06:59:05
    Author     : CYUTIM
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navbar.jspf" %>

        <%@include file="/WEB-INF/jspf/jumbotron.jspf" %>
        <div class="container">
            <h2>DVD Library Application</h2><br/>
            
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>


    </body>
</html>
