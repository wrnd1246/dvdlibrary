<%@page import="com.dvd.model.DVDItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include  file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navbar.jspf" %>

        <div class="container">
            <div class="row">
                <h1>Set Display Preferences</h1>
                <form action="set_prefs.do" method="POST">
                    Columns to display in list: <br><br>
                    <label class="cbx-label" for="Title">Title</label>
                    <input id="Title" type="checkbox" name="show" value="showTitle"/>
                    <label class="cbx-label" for="Title">Year</label>
                    <input id="Year" type="checkbox" name="show" value="showYear"/>
                    <label class="cbx-label" for="Title">Genre</label>
                    <input id="Genre" type="checkbox" name="show" value="showGenre"/><br><br>
                    <input class="btn btn-primary" type="submit" value="Set Preferences" />
                </form>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
</html>

