<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include  file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navbar.jspf" %>

        <div class="container">
            <div class="row">
                <h3 class="text-info">新增成功！</h3> <br><br/>
                Title: ${DVDItem.getTitle()}<br><br/>
                Year : ${DVDItem.getYear()}<br><br/>
                Genre: ${DVDItem.getGenre()}<br><br/><br/>
                為您的DVD增加封面！<br><br/>
                <form ENCTYPE="multipart/form-data" method="POST" action="fileServlet">
                    <div class="form-group">     
                        <input type="file" name="file"/><br><br/>
                    </div>

                    <input class="btn btn-primary" type="submit" value="Upload" />
                </form>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
</html>
