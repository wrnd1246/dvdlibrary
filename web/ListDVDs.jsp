<%@page import="com.dvd.model.DVDItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include  file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navbar.jspf" %>

        <div class="container">
            <div class="row">
                Hello!<b>${username}</b> Welcome!
                You currently have <b> ${dvds_size}</b>
                DVDs in your collection:<br><br>
                <table class="table">
                    <!-- column headers -->


                    <th>ID</th>
                        ${showTitle == "true" ? '<th>Title</th><th>Cover</th>' : ''}
                        ${showYear == "true" ? '<th>Year</th>' : ''}
                        ${showGenre == "true" ? '<th>Genre</th>' : ''}
                    
                    <!-- column data -->
                    <c:forEach var="row" begin="0" end="${dvds_size-1}">
                        <tr>
                            <td>${list_I[row]}</td>    
                            <c:if test='${showTitle == "true"}'>  
                                <td>${list_T[row]}</td>  
                                <td><img src='image/${list_img[row]}' width='200'></td>
                                </c:if>  
                                <c:if test='${showYear == "true"}'>  
                                <td>${list_Y[row]}</td>        
                            </c:if>  
                            <c:if test='${showGenre == "true"}'>  
                                <td>${list_G[row]}</td>        
                            </c:if>    
                        </tr>
                    </c:forEach>


                </table>
                <a class="btn btn-primary" href="<c:url value='/index.jsp'/>">回到首頁</a><br>        
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
</html>

