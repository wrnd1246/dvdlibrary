<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include  file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navbar.jspf" %>

        <div class="container">
            <div class="row">
                <h3 class="text-info">${upload_message}</h3> <br><br/>
                <img src="ShowContentServlet" width="200"/><br><br/>
                <a class="btn btn-primary" href='ListLibraryServlet'>顯示DVD清單</a><br>        
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
</html>
