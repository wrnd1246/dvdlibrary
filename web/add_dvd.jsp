<%-- 
    Document   : AddLeague
    Created on : 2015/12/9, 下午 08:28:18
    Author     : CYUTIM
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include  file="/WEB-INF/jspf/head.jspf" %>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navbar.jspf" %>

        <div class="container">
            <div class="row">

                <ul>
                    <c:forEach items="${errMsg}" var="msg">
                        <li><font color='red'>${msg}</font></li><br>
                    </c:forEach>
                </ul>

                <% String genres = config.getInitParameter("genres");%>

                <form action="<c:url value='/add_dvd.do'/>" method="post">

                    <div class="form-group">     
                        <label for="titleInput">Title: </label> 
                        <input id="titleInput" class="form-control" name="title" type="text" size="20" value="${param.title}"/><br> <br/>
                    </div>
                    
                    <div class="form-group">
                        <label for="yearInput">Year:</label> 
                        <input id="yearInput" class="form-control" 
                               name="year" type="text" size="20" value="${param.year}"/> <br> <br/>
                    </div>
                    
                    <div class="form-group">
                        <label for="genreInput">Genre:</label> 
                        <select id="genreInput" class="form-control" name="genre">
                            <option value="UNKNOW" ${(param.genre==null ? "selected" : "")} >Unknown...</option>
                            <c:forTokens items="<%=genres%>" var="s" delims=",">
                                <c:set var="genreStr" value="${fn:trim(s)}"/>
                                <option value="${s}" ${(genreStr==param.genre)? "selected" : ""} >${s}</option>
                            </c:forTokens>
                        </select> <br><br/>
                    </div>

                    <div class ="form-group">
                        <img src='CreateRandImg.view'><br/>
                        <label for="codeInput">請輸入驗證碼：</label>
                        <input id="codeInput" class="form-control" 
                               name="code" type="text" size="20" value="${param.code}"/> <br> <br/>       
                    </div>

                    <input class="btn btn-primary" type="submit" value="Add DVD" />
                </form>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/footer.jspf" %>
</html>
